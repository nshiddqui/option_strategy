<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Comment Entity
 *
 * @property int $id
 * @property string $comment
 * @property int|null $parent_id
 * @property int|null $lft
 * @property int|null $rght
 * @property int $user_id
 * @property int $blog_id
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime|null $modified
 *
 * @property \App\Model\Entity\ParentComment $parent_comment
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Blog $blog
 * @property \App\Model\Entity\ChildComment[] $child_comments
 */
class Comment extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'comment' => true,
        'parent_id' => true,
        'lft' => true,
        'rght' => true,
        'user_id' => true,
        'blog_id' => true,
        'created' => true,
        'modified' => true,
        'parent_comment' => true,
        'user' => true,
        'blog' => true,
        'child_comments' => true
    ];
}

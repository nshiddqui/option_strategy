<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Hash Entity
 *
 * @property int $id
 * @property string $value
 * @property string $md5_hash
 * @property string $sha1_hash
 * @property int|null $rank
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 */
class Hash extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'value' => true,
        'md5_hash' => true,
        'sha1_hash' => true,
        'rank' => true,
        'created' => true,
        'modified' => true
    ];
}

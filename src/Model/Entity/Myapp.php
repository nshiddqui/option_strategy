<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * App Entity
 *
 * @property int $id
 * @property string $content
 * @property string $title
 * @property string $controller
 * @property string $action
 * @property string|null $variable
 * @property int $views
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 */
class Myapp extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'content' => true,
        'title' => true,
        'controller' => true,
        'action' => true,
        'variable' => true,
        'meta' => true,
        'views' => true,
        'created' => true,
        'modified' => true
    ];
}

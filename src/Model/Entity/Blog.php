<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Blog Entity
 *
 * @property int $id
 * @property string $title
 * @property string $content
 * @property string|null $youtube_link
 * @property string|null $download_link
 * @property int|null $topic_id
 * @property int $user_id
 * @property int $views
 * @property string $slug
 * @property int $status
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime|null $modified
 *
 * @property \App\Model\Entity\Topic $topic
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Comment[] $comments
 */
class Blog extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'title' => true,
        'content' => true,
        'youtube_link' => true,
        'download_link' => true,
        'position' => true,
        'topic_id' => true,
        'user_id' => true,
        'views' => true,
        'slug' => true,
        'status' => true,
        'created' => true,
        'modified' => true,
        'topic' => true,
        'user' => true,
        'comments' => true
    ];
}

<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Hashes Model
 *
 * @method \App\Model\Entity\Hash get($primaryKey, $options = [])
 * @method \App\Model\Entity\Hash newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Hash[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Hash|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Hash|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Hash patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Hash[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Hash findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class HashesTable extends Table {

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        parent::initialize($config);

        $this->setTable('hashes');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator) {
        $validator
                ->integer('id')
                ->allowEmpty('id', 'create');

        $validator
                ->scalar('value')
                ->maxLength('value', 255)
                ->requirePresence('value', 'create')
                ->notEmpty('value');

        $validator
                ->scalar('md5_hash')
                ->maxLength('md5_hash', 32)
                ->requirePresence('md5_hash', 'create')
                ->notEmpty('md5_hash')
                ->add('md5_hash', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
                ->scalar('sha1_hash')
                ->maxLength('sha1_hash', 40)
                ->requirePresence('sha1_hash', 'create')
                ->notEmpty('sha1_hash')
                ->add('sha1_hash', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
                ->integer('rank')
                ->allowEmpty('rank');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules) {
        $rules->add($rules->isUnique(['md5_hash']));
        $rules->add($rules->isUnique(['sha1_hash']));

        return $rules;
    }

}

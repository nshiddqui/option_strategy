<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Http\Exception\NotFoundException;

class StockMarketsController extends AppController {

    public function beforeFilter(\Cake\Event\Event $event) {
        parent::beforeFilter($event);
        $this->loadModel('Myapps');
    }

    public function index() {
        $this->viewBuilder()->setLayout(false);
        $appView = $this->Myapps->get(8);
        $appView->views = $appView->views + 1;
        $this->Myapps->save($appView);
    }

    public function getData() {
        if ($this->Auth->user()) {
            $this->loadModel('Trades');
            $data = $this->Trades->find('all', [
                'conditions' => [
                    'user_id' => $this->Auth->user('id')
                ]
            ]);
        } else {
            $data = [];
        }
        $this->set(compact('data'));
        $this->set('_serialize', 'data');
        $this->RequestHandler->renderAs($this, 'json');
    }

    public function addToDatabase() {
        $data = [
            'status' => false
        ];
        if ($this->Auth->user()) {
            $this->loadModel('Trades');
            $getData = $this->request->getQuery();
            $getData['user_id'] = $this->Auth->user('id');
            $trade = $this->Trades->newEntity();
            $data = $this->Trades->patchEntity($trade, $getData);
            if ($this->Trades->save($data)) {
                $data['status'] = true;
            }
        }
        $this->set(compact('data'));
        $this->set('_serialize', 'data');
        $this->RequestHandler->renderAs($this, 'json');
    }

    public function removeFromDatabase($id) {
        $data = [
            'status' => false
        ];
        if ($this->Auth->user()) {
            $this->loadModel('Trades');
            $trade = $this->Trades->get($id, [
                'conditions' => [
                    'user_id' => $this->Auth->user('id')
                ]
            ]);
            if ($this->Trades->delete($trade)) {
                $data['status'] = true;
            }
        }
        $this->set(compact('data'));
        $this->set('_serialize', 'data');
        $this->RequestHandler->renderAs($this, 'json');
    }

}

<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Http\Exception\NotFoundException;

class BeautifierController extends AppController {
    public function beforeFilter(\Cake\Event\Event $event) {
        parent::beforeFilter($event);
        $this->loadModel('Myapps');
    }

    public function jsonParser() {
        $appView = $this->Myapps->get(5);
        $appView->views = $appView->views+1;
        $this->Myapps->save($appView);
        $meta = $appView->meta;
        $this->set(compact('meta'));
        $this->set('customAds',true);
    }

}

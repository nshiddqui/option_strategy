<?php

namespace App\Controller;

use App\Controller\AppController;
use GeoIp2\Database\Reader;


/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 *
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ipinfoController extends AppController {

    public function beforeFilter(\Cake\Event\Event $event) {
        parent::beforeFilter($event);
        $this->loadModel('Myapps');
    }

    public function index() {
        $reader = new Reader(WWW_ROOT . 'geo_ip/GeoLite2-City_20190409/GeoLite2-City.mmdb');
        $ip = $this->request->getQuery('ip');
        if($ip == null){
            $ip = $this->request->clientIp();
        }

        try {
            $record = $reader->city($ip);
            /*$encode = json_encode($record);
            $decode = json_decode($encode, true);
            $found_key = array_search('geoname_id', $decode);
            var_dump($found_key);
            die();*/
            /*foreach ($decode as $k1 => $v1) {
                if (is_array($v1)) {
                    foreach ($v1 as $k2 => $v2) {
                        if (is_array($v2)) {

                        }else{

                        }
                    }
                }
            }*/
        } catch (\Exception $e) {
            $record = false;
        }

        if ($record) {
            $subdivisions = [];
            foreach ($record->subdivisions as $div) {
                $subdivisions[] = $div->names['en'];
            }
            $ipDetail = [
                'IP' => $ip,
                'city' => $record->city->name,
                'location' => [
                    'accuracy_radius' => $record->location->accuracyRadius,
                    'latitude' => $record->location->latitude,
                    'longitude' => $record->location->longitude,
                    'time_zone' => $record->location->timeZone,
                ],
                'postal' => $record->postal->code,
                'sub_divisions' => $subdivisions,
                'continent' => [
                    'code' => $record->continent->code,
                    'names' => $record->continent->names['en'],
                ],
                'country' => [
                    'ISO_code' => $record->country->isoCode,
                    'names' => $record->country->names['en'],
                ]
            ];
        }else{
            $ipDetail = false;
        }
        $appView = $this->Myapps->get(7);
        $appView->views = $appView->views + 1;
        $this->Myapps->save($appView);
        
        $allApps = $this->Myapps->find('all', ['conditions' => ['id !=' => 7], 'order' => 'views DESC']);
        $appData = $this->Myapps->find('all', ['conditions' => ['id' => 7]])->first();
        $meta = $appData->meta;
        $this->set(compact('allApps', 'appData', 'meta','ipDetail','ip'));
    }
}

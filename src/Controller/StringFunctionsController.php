<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Http\Exception\NotFoundException;

class StringFunctionsController extends AppController {

    public function beforeFilter(\Cake\Event\Event $event) {
        parent::beforeFilter($event);
        $this->loadModel('Myapps');
    }

    public function countLength() {
        if ($this->request->is(['ajax', 'post'])) {
            $whitespace = $this->request->getData('whitespace');
            $string = $this->request->getData('inputString');
            if (empty($string) || trim($string) == "") {
                $response['totalWord'] = 0;
                $response['length'] = 0;
                echo json_encode($response);
                die();
            }
            $response['totalWord'] = $this->get_num_of_words($string);
            if ($whitespace == "0") {
                $string = preg_replace('/\s+/', '', $string);
            }
            $response['length'] = strlen($string);
            echo json_encode($response);
            die();
        } else {
            $appView = $this->Myapps->get(3);
            $appView->views = $appView->views + 1;
            $this->Myapps->save($appView);
        }
        $allApps = $this->Myapps->find('all', ['conditions' => ['id !=' => 3],'order'=>'views DESC','limit' => 4]);
        $appData = $this->Myapps->find('all', ['conditions' => ['id' => 3]])->first();
        $meta = $appData->meta;
        $this->set(compact('allApps', 'appData','meta'));
    }

    public function reverseString() {
        $appView = $this->Myapps->get(4);
        $appView->views = $appView->views + 1;
        $this->Myapps->save($appView);
        $allApps = $this->Myapps->find('all', ['conditions' => ['id !=' => 4],'order'=>'views DESC', 'limit' => 4]);
        $appData = $this->Myapps->find('all', ['conditions' => ['id' => 4]])->first();
        $meta = $appData->meta;
        $this->set(compact('allApps', 'appData','meta'));
    }

    public function get_num_of_words($string) {
        $string = preg_replace('/\s+/', ' ', trim($string));
        $words = explode(" ", $string);
        return count($words);
    }

}

?>
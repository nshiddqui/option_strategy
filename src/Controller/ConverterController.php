<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Http\Exception\NotFoundException;

class ConverterController extends AppController {

    public function beforeFilter(\Cake\Event\Event $event) {
        parent::beforeFilter($event);
        $this->loadModel('Myapps');
    }

    public function jsonToPhpArray() {
        if ($this->request->is(['ajax', 'post'])) {
            $jsonInput = $this->request->getData('inputJson');
            $isJson = $this->isJson($jsonInput);
            if ($isJson) {
                $jsonArray = json_decode($jsonInput, true);
                $message = ["status" => true, "message" => "Your JSON code is valid."];
                $this->set(compact('message'));
                $this->set(compact('jsonArray'));
            } else {
                $message = ["status" => false, "message" => "This is not a valid JSON. Please provide a valid JSON to decode."];
                $this->set(compact('message'));
            }
        } else {
            $appView = $this->Myapps->get(6);
            $appView->views = $appView->views + 1;
            $this->Myapps->save($appView);
        }
        $allApps = $this->Myapps->find('all', ['conditions' => ['id !=' => 6],'order'=>'views DESC','limit' => 4]);
        $appData = $this->Myapps->find('all', ['conditions' => ['id' => 6]])->first();
        $meta = $appData->meta;
        $this->set(compact('allApps', 'appData','meta'));
    }

    public function isJson($string) {
        if (is_string($string)) {
            json_decode($string);
            return (json_last_error() == JSON_ERROR_NONE);
        } else {
            return false;
        }
    }

}

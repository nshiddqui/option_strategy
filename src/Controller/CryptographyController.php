<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Http\Exception\NotFoundException;

class CryptographyController extends AppController {
    //public $helpers = ['Content'];

    public function beforeFilter(\Cake\Event\Event $event) {
        parent::beforeFilter($event);
        $this->loadModel('Hashes');
        $this->loadModel('Myapps');
    }

    public function md5() {
        if ($this->request->is('post')) {
            $this->autoRender = false;
            $type = $this->request->getData('type');
            $input = $this->request->getData('inputString');
            if ($type == 'encrypt') {
                $md5 = md5($input);
                $sha1 = sha1($input);
                $isHashFound = $this->Hashes->findByMd5Hash($md5)->first();
                if ($isHashFound == NULL) {
                    $hash = $this->Hashes->newEntity();
                    $hash = $this->Hashes->patchEntity($hash, array('value' => $input, 'md5_hash' => $md5, 'sha1_hash' => $sha1, 'rank' => 0));
                    $this->Hashes->save($hash);
                    echo json_encode(array('status' => true, 'md5' => $md5));
                    die();
                }
                $query = $this->Hashes->query();
                $query->update()
                        ->set(array('rank' => $isHashFound->rank + 1))
                        ->where(['id' => $isHashFound->id])
                        ->execute();
                echo json_encode(array('status' => true, 'md5' => $md5));
                die();
            }
            if ($type == 'decrypt') {
                $md5 = $input;
                if (strlen($md5) != 32) {
                    echo json_encode(array('status' => false, 'message' => 'Please provide valid md5 hash.'));
                    die();
                }
                $isHashFound = $this->Hashes->findByMd5Hash($md5)->first();

                if ($isHashFound == NULL) {
                    $this->loadModel('NotFoundHashes');
                    $hash = $this->NotFoundHashes->newEntity();
                    $hash = $this->NotFoundHashes->patchEntity($hash, array('value' => $input));
                    if ($hashId = $this->NotFoundHashes->save($hash)) {
                        echo json_encode(array('status' => false, 'id' => $hashId->id, 'hash' => $md5, 'message' => 'Sorry, this hash is not in our local database. Enter your email to be notified when we crack this hash (full search in background): '));
                        die();
                    } else {
                        $this->log("[Error] " . $input . ' Not found hash not saved.');
                        echo json_encode(array('status' => false, 'message' => 'Unable to decode hash. Please try later.'));
                        die();
                    }
                }
                $query = $this->Hashes->query();
                $query->update()
                        ->set(array('rank' => $isHashFound->rank + 1))
                        ->where(['id' => $isHashFound->id])
                        ->execute();
                echo json_encode(array('status' => true, 'md5' => $isHashFound->value));
                die();
            }
            echo json_encode(array('status' => false, 'message' => 'Invalid request'));
            die();
        }else{
            $appView = $this->Myapps->get(1);
            $appView->views = $appView->views + 1;
            $this->Myapps->save($appView);
        }
        $allApps = $this->Myapps->find('all',['conditions'=>['id !='=>1],'order'=>'views DESC']);
        $appData = $this->Myapps->find('all',['conditions'=>['id'=>1]])->first();
        $meta = $appData->meta;
        $this->set(compact('allApps','appData','meta'));
        
    }

    public function sha1() {
        if ($this->request->is(array('ajax', 'post'))) {
            $this->loadModel('Hashes');
            $this->autoRender = false;
            $type = $this->request->getData('type');
            $input = $this->request->getData('inputString');

            if ($type == 'encrypt') {
                $md5 = md5($input);
                $sha1 = sha1($input);
                $isHashFound = $this->Hashes->findBySha1Hash($sha1)->first();
                if ($isHashFound == NULL) {
                    $hash = $this->Hashes->newEntity();
                    $hash = $this->Hashes->patchEntity($hash, array('value' => $input, 'md5_hash' => $md5, 'sha1_hash' => $sha1, 'rank' => 0));
                    if ($this->Hashes->save($hash)) {
                        echo json_encode(array('status' => true, 'sha1' => $sha1));
                        die();
                    } else {
                        $this->log("[Error] " . $input . ' unable to hash please check');
                        echo json_encode(array('status' => false, 'message' => 'Unable to encode hash. Please try later.'));
                        die();
                    }
                }
                $query = $this->Hashes->query();
                $query->update()
                        ->set(array('rank' => $isHashFound->rank + 1))
                        ->where(['id' => $isHashFound->id])
                        ->execute();
                echo json_encode(array('status' => true, 'sha1' => $sha1));
                die();
            }
            if ($type == 'decrypt') {
                $sha1 = $input;
                if (strlen($sha1) != 40) {
                    echo json_encode(array('status' => false, 'message' => 'Please provide valid sha1 hash.'));
                    die();
                }
                $isHashFound = $this->Hashes->findBySha1Hash($sha1)->first();

                if ($isHashFound == NULL) {
                    $this->loadModel('NotFoundHashes');
                    $hash = $this->NotFoundHashes->newEntity();
                    $hash = $this->NotFoundHashes->patchEntity($hash, array('value' => $input));
                    if ($hashId = $this->NotFoundHashes->save($hash)) {
                        echo json_encode(array('status' => false, 'id' => $hashId->id, 'hash' => $sha1, 'message' => 'Sorry, this hash is not in our local database. Enter your email to be notified when we crack this hash (full search in background): '));
                        die();
                    } else {
                        $this->log("[Error] " . $input . ' Not found hash not saved.');
                        echo json_encode(array('status' => false, 'message' => 'Unable to decode hash. Please try later.'));
                        die();
                    }
                }
                $query = $this->Hashes->query();
                $query->update()
                        ->set(array('rank' => $isHashFound->rank + 1))
                        ->where(['id' => $isHashFound->id])
                        ->execute();
                echo json_encode(array('status' => true, 'sha1' => $isHashFound->value));
                die();
            }
            echo json_encode(array('status' => false, 'message' => 'Invalid request'));
            die();
        }else{
            $appView = $this->Myapps->get(2);
            $appView->views = $appView->views + 1;
            $this->Myapps->save($appView);
        }
        $allApps = $this->Myapps->find('all',['conditions'=>['id !='=>2],'order'=>'views DESC']);
        $appData = $this->Myapps->find('all',['conditions'=>['id'=>2]])->first();
        $meta = $appData->meta;
        $this->set(compact('allApps','appData','meta'));
        
    }

    public function hashNotFoundEmail() {
        $this->loadModel('NotFoundHashes');
        $isValidData = $this->NotFoundHashes->find('all', array('conditions' => array('id' => $this->request->getQuery('requestid'), 'value' => $this->request->getQuery('inputString'))))->first();
        if ($isValidData == NULL || count($isValidData) == 0 || $this->request->getQuery('email') == '' || $this->request->getQuery('email') == NULL) {
            $this->Flash->error(__('Invalid Request'));
            if ($this->referer() != NULL) {
                return $this->redirect($this->referer());
            } else {
                return $this->redirect();
            }
        }
        $query = $this->NotFoundHashes->query();
        $query->update()
                ->set(array('email' => $this->request->getQuery('email')))
                ->where(['id' => $isValidData->id])
                ->execute();
        $this->Flash->success(__('Thank you! We will notify you soon on your email address ' . $this->request->getQuery('email')));
        if ($this->referer() != NULL) {
            return $this->redirect($this->referer());
        } else {
            return $this->redirect();
        }
        die();
    }

}

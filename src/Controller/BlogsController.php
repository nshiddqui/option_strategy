<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Http\Exception\NotFoundException;

/**
 * Blogs Controller
 *
 * @property \App\Model\Table\BlogsTable $Blogs
 *
 * @method \App\Model\Entity\Blog[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class BlogsController extends AppController {

    public function index() {
        $this->paginate = [
            'conditions'=>['status' => 1],
            'contain' => ['Topics', 'Users']
        ];
        $blogs = $this->paginate($this->Blogs);
        $this->set(compact('blogs'));
    }

    public function detail($slug) {
        $blog = $this->Blogs->find('all', [
                    'conditions' => ['slug' => $slug, 'status' => 1],
                    'contain' => ['Comments']
                ])->first();
        if ($blog == NULL) {
            throw new NotFoundException(__('404'));
        }
        $this->loadModel('Myapps');
        $allApps = $this->Myapps->find('all', ['order' => 'views DESC', 'limit' => 6]);
        //related blogs
        $relatedBlog = $this->Blogs->find('all', ['conditions' => ['topic_id' => $blog->topic_id, 'status' => 1], 'order' => 'position DESC']);
        $meta = null; //$blog->meta;
        $this->set(compact('blog', 'relatedBlog', 'meta', 'allApps'));
    }

}

<!DOCTYPE html>
<html>
    <head>
        <?= $this->Html->charset() ?>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
        <meta http-equiv="content-language" content="en-US">
        <meta name="google-site-verification" content="kQpNf6P6pPuUdXLyfIjdUeJ3bHxIm9z1LDO97jCh-MY" />
        <meta name="robots" content="INDEX, FOLLOW" />
        <?php
        echo $this->Html->meta('url', $this->Url->build(null, true));
        echo $this->Html->meta('author', 'Rafi Ahmad');
        
        if (isset($meta) && $meta != NULL && trim($meta) != "") {
            $tags = explode(";", $meta);
            foreach ($tags as $tag) {
                $tagSeprate = explode(":", $tag);
                if (isset($tagSeprate[1])) {
                    if(trim($tagSeprate[0]) == 'image'){
                        echo $this->Html->meta(trim($tagSeprate[0]), $html->Url->image(trim($tagSeprate[1]),['fullBase' => true]));
                    }else{
                        echo $this->Html->meta(trim($tagSeprate[0]), trim($tagSeprate[1]));
                    }
                    if(trim($tagSeprate[0]) == 'title'){
                        $title = $tagSeprate[1];
                    }
                }
            }
        }
        ?>
        <title>
            <?= isset($title) ? $title : $this->fetch('title') ?>
        </title>



        <?= $this->Html->meta('icon') ?>

        <?= $this->Html->css('bootstrap4.1.3.min.css') ?>
        <?= $this->Html->css('style.css') ?>
        <?= $this->Html->script('jquery3.3.1.min.js') ?>
        <?= $this->Html->script('bootstrap4.1.3.min.js') ?>

        <?= $this->fetch('meta') ?>
        <?= $this->fetch('css') ?>
        <?= $this->fetch('script') ?>
        <?php if ($_SERVER['REMOTE_ADDR'] != "127.0.0.1" && $_SERVER['REMOTE_ADDR'] != "::1" && $_SERVER['REMOTE_ADDR'] != '45.122.120.20') { ?>
            <!-- Global site tag (gtag.js) - Google Analytics -->
            <script async src="https://www.googletagmanager.com/gtag/js?id=UA-69901653-4"></script>
            <script>
                window.dataLayer = window.dataLayer || [];
                function gtag() {
                    dataLayer.push(arguments);
                }
                gtag('js', new Date());

                gtag('config', 'UA-69901653-4');
            </script>
            <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
            <?php if (isset($customAds)) { ?>
            <?php } else { ?>
                <script>
                    (adsbygoogle = window.adsbygoogle || []).push({
                        google_ad_client: "ca-pub-9302256880608736",
                        enable_page_level_ads: true
                    });
                </script>
                <script async custom-element="amp-auto-ads"
                        src="https://cdn.ampproject.org/v0/amp-auto-ads-0.1.js">
                </script>
            <?php } ?>
        <?php } ?>
    </head>
    <body>
        <nav class="navbar navbar-expand-sm bg-dark navbar-dark">
            <!-- Brand/logo -->
            <?php
            echo $this->Html->link(
                    $this->Html->image('logo/logo-yellow.png', ['class' => 'img-responsive', 'alt' => 'myCode myWay',]), ['controller' => 'Pages', 'action' => 'index', 'prefix' => false], ['escape' => false, 'class' => 'navbar-brand']
            );
            ?>

        </nav>
        <div class="container-fluid my-4">
            <?= $this->fetch('content') ?>
        </div>
    </body>
</html>

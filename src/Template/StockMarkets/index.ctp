<!DOCTYPE html>
<html lang="en">

    <head>
        <title>Option Strategy Calculator</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
        <script src="https://use.fontawesome.com/e427379d7f.js"></script>
        <style>
            tr>th {
                text-align: center;
            }

            .ponter {
                cursor: pointer;
            }

            .border1 {
                border: 1px solid grey;
            }
            .lock-icon{
                margin: 5px;
                cursor: pointer;
                font-size: 20px;
                position: absolute;
            }
        </style>
    </head>


    <body>

        <div class="container">
            <div class="row">
                <h2>Option Strategy Calculator</h2>

                <div class="col-md-3">
                    <div class="form-group">
                        <label for="startStrikePrice">Strike Price Start:</label>
                        <input type="text" class="form-control" id="startStrikePrice">
                    </div>

                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="priceDifference">Price Difference:</label>
                        <input type="text" class="form-control" id="priceDifference">
                    </div>

                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="totalNumberOfShareIn1Lot">Total Number Of Share 1 Lot:</label>
                        <input type="text" class="form-control" id="totalNumberOfShareIn1Lot">
                    </div>

                </div>


            </div>




            <div class="row">
                <h3>Add Trade</h3>
                <div class="col-md-12 text-center" id="addTradeAllStrikes">

                </div>
            </div>




            <h3>Trades:</h3>

            <table class="table table-bordered text-center" id="tradesList">

            </table>


            <h3>Complete Details(PNL):</h3>

            <table class="table table-bordered text-center" id="tradespnl">

            </table>
        </div>


        <!-- Modal -->
        <div id="addTradeModal" class="modal fade" role="dialog">
            <div class="modal-dialog modal-lg">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Modal Header</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">

                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="strikePrice">Strike Price:</label>
                                    <input type="text" class="form-control" id="strikePrice">
                                </div>

                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="ltp">LTP:</label>
                                    <input type="text" class="form-control" id="ltp">
                                </div>

                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="buy_short">Buy/Short:</label>
                                    <select class="form-control" id="buy_short">
                                        <option value="BUY">BUY</option>
                                        <option value="SHORT">SHORT</option>
                                    </select>
                                </div>

                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="ce_pe">CE/PE</label>
                                    <select class="form-control" id="ce_pe">
                                        <option value="CE">CE</option>
                                        <option value="PE">PE</option>
                                    </select>
                                </div>

                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label for="numberOfLot">Number Of Lot:</label>
                                    <input type="text" class="form-control" value="1" id="numberOfLot">
                                </div>

                            </div>

                            <div class="col-md-2">
                                <div class="form-group">
                                    <button type="button" id="addNewTrade" style="margin-top: 22px;"
                                            class="btn btn-success">Add</button>
                                </div>
                            </div>


                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div>

    </body>


    <script>
        function referrerData() {
            var str = '<?php echo $this->request->getQuery('referrer_data'); ?>';
            try {
                JSON.parse(str);
            } catch (e) {
                return false;
            }
            return JSON.parse(str);
        }

        if (referrerData() !== false) {
            data = referrerData();
            localStorage.setItem("totalNumberOfShareIn1Lot", data.totalNumberOfShareIn1Lot);
            localStorage.setItem("startStrikePrice", data.startStrikePrice);
            $.each(data.data, function (k, data) {
                addToLocaTrade(data);
            });
            $(location).attr('href', 'http://' + document.location.host + '/stock-markets')
        }




        $(document).ready(function () {
            getExistData();
            main();
        });
        function getExistData() {
            trades = localStorage.getItem("trades");
            if (trades !== null) {
                trades = JSON.parse(trades);
            } else {
                trades = [];
            }
            $.get('<?= $this->Url->build(['action' => 'getData']) ?>', function (data) {
                $.each(data, function (k, v) {
                    var data = [v.strikePrice, v.ltp, v.buy_short, v.ce_pe, v.numberOfLot, v.preChecked, v.id];
                    var exists = false;
                    $.each(trades, function (i, j) {
                        if (j[6] && j[6] === v.id) {
                            exists = true;
                            trades[i] = data;
                        }
                    });
                    if (exists === false) {
                        addToLocaTrade(data);
                    }
                });
                localStorage.setItem("trades", JSON.stringify(trades));
                main();
            });
        }
        function plusbutton(index) {
            trades = localStorage.getItem("trades");
            if (trades === null) {
                trades = [];
            } else {
                trades = JSON.parse(trades);
            }
            // Get the field name
            tradeIndexNumber = parseInt(index);
            newValue = parseInt(trades[tradeIndexNumber][4]) + 1;
            if (newValue > 0) {
                trades[tradeIndexNumber][4] = newValue;
                $("#trade-lot-input-" + tradeIndexNumber.toString()).val(newValue);
            }
            localStorage.setItem("trades", JSON.stringify(trades));
            main();
        }
        function minusbutton(index) {
            trades = localStorage.getItem("trades");
            if (trades === null) {
                trades = [];
            } else {
                trades = JSON.parse(trades);
            }
            tradeIndexNumber = parseInt(index);
            newValue = parseInt(trades[tradeIndexNumber][4]) - 1;
            if (newValue > 0) {
                trades[tradeIndexNumber][4] = newValue;
                $("#trade-lot-input-" + tradeIndexNumber.toString()).val(newValue);
            }
            localStorage.setItem("trades", JSON.stringify(trades));
            main();
        }



        function inCalculation(selector) {
            indexVal = $(selector).val()
            trades = localStorage.getItem("trades");
            if (trades === null) {
                trades = [];
            } else {
                trades = JSON.parse(trades);
            }
            if ($(selector).prop("checked") == true) {
                trades[indexVal][5] = true;
            } else {
                trades[indexVal][5] = false;
            }
            localStorage.setItem("trades", JSON.stringify(trades));
            main();

        }

        function changeltp(index) {
            trades = localStorage.getItem("trades");
            if (trades === null) {
                trades = [];
            } else {
                trades = JSON.parse(trades);
            }
            tradeIndexNumber = parseInt(index);
            newltp = parseFloat($("#trade-ltp-" + index).val());
            if (isNaN(newltp) === false) {
                trades[tradeIndexNumber][1] = newltp;
                localStorage.setItem("trades", JSON.stringify(trades));
                main();
            }
        }
        function changeBuyShort(index) {
            trades = localStorage.getItem("trades");
            if (trades === null) {
                trades = [];
            } else {
                trades = JSON.parse(trades);
            }
            tradeIndexNumber = parseInt(index);
            newBuyShort = $("#trade-buy-short-" + index).val();
            trades[tradeIndexNumber][2] = newBuyShort;
            localStorage.setItem("trades", JSON.stringify(trades));
            main();

        }

        function addTradeModal(strike) {
            $('#addTradeModal').modal('show');
            $("#strikePrice").val(strike);
        }

        function addToLocaTrade(newTrade) {
            trades = localStorage.getItem("trades");
            if (trades === null) {
                trades = [];
            } else {
                trades = JSON.parse(trades);
            }
            trades.push(newTrade)
            localStorage.setItem("trades", JSON.stringify(trades));
        }

        function deleteTrade(key) {
            trades = localStorage.getItem("trades");
            if (trades === null) {
                trades = [];
            } else {
                trades = JSON.parse(trades);
            }
            if (trades[key][6] && trades[key][6] !== false) {
                $.get('<?= $this->Url->build(['action' => 'removeFromDatabase']) ?>/' + trades[key][6], function (response) {
                    if (response.status === true) {
                        trades.splice(key, 1);
                        localStorage.setItem("trades", JSON.stringify(trades));
                        main();
                    } else {
                        alert('Unable to delete your data');
                    }
                });
            } else {
                trades.splice(key, 1);
                localStorage.setItem("trades", JSON.stringify(trades));
                main();
            }
        }


        $("#addNewTrade").on('click', function () {
            var strikePrice = $("#strikePrice").val();
            if (strikePrice.trim() === "") {
                return false;
            }
            var ltp = $("#ltp").val();
            if (ltp.trim() === "") {
                return false;
            }
            var buy_short = $("#buy_short option:selected").text();
            if (buy_short.trim() === "") {
                return false;
            }
            var ce_pe = $("#ce_pe option:selected").text();
            if (ce_pe.trim() === "") {
                return false;
            }
            var numberOfLot = $("#numberOfLot").val();
            if (numberOfLot.trim() === "") {
                return false;
            }
            //var totalNumberOfShareIn1Lot = localStorage.getItem("totalNumberOfShareIn1Lot");
            //ltp = numberOfLot * ltp;
            //var totalShares = parseInt(numberOfLot * totalNumberOfShareIn1Lot);
            var preChecked = true
            addToLocaTrade([strikePrice, ltp, buy_short, ce_pe, numberOfLot, preChecked]);
            //$("#strikePrice").val('');
            $("#ltp").val('');
            $('#addTradeModal').modal('hide');
            main();

            //console.log(strikePrice,ltp,buy_short,ce_pe,numberOfLot);

        });





        $("#startStrikePrice").on('change', function () {
            localStorage.setItem("startStrikePrice", $(this).val());
            main();
        });
        $("#priceDifference").on('change', function () {
            localStorage.setItem("priceDifference", $(this).val());
            main();
        });
        $("#totalNumberOfShareIn1Lot").on('change', function () {
            localStorage.setItem("totalNumberOfShareIn1Lot", $(this).val());
            main();
        });

        function addToDatabase(key) {
            var trades = localStorage.getItem("trades");
            if (trades !== null) {
                trades = JSON.parse(trades);
                $.get('<?= $this->Url->build(['action' => 'addToDatabase']) ?>', {
                    strikePrice: trades[key][0],
                    ltp: trades[key][1],
                    buy_short: trades[key][2],
                    ce_pe: trades[key][3],
                    numberOfLot: trades[key][4],
                    preChecked: trades[key][5]
                }, function (response) {
                    if (response.status === true) {
                        trades[key][6] = response.id;
                        localStorage.setItem("trades", JSON.stringify(trades));
                        main();
                    } else {
                        alert('Unable to lock your data');
                    }
                });
            }
        }

        function removeFromDatabase(key) {
            var trades = localStorage.getItem("trades");
            if (trades !== null) {
                trades = JSON.parse(trades);
                $.get('<?= $this->Url->build(['action' => 'removeFromDatabase']) ?>/' + trades[key][6], function (response) {
                    if (response.status === true) {
                        trades[key][6] = false;
                        localStorage.setItem("trades", JSON.stringify(trades));
                        main();
                    } else {
                        alert('Unable to lock your data');
                    }
                });
            }
        }

        // main function should responsible for createing tables and calculation used in localstorage
        function main() {
            //localStorage.removeItem("trades");


            var startStrikePrice = localStorage.getItem("startStrikePrice");
            if (startStrikePrice === null) {
                startStrikePrice = 10300;
            }
            $("#startStrikePrice").val(startStrikePrice);

            var priceDifference = localStorage.getItem("priceDifference");
            if (priceDifference === null) {
                priceDifference = 100;
            }
            $("#priceDifference").val(priceDifference);

            var totalNumberOfShareIn1Lot = localStorage.getItem("totalNumberOfShareIn1Lot");
            $("#totalNumberOfShareIn1Lot").val(totalNumberOfShareIn1Lot);


            strikes = [];
            pnl = []
            strikes.push("Strike Price");
            lastStrikePrice = parseInt(startStrikePrice);
            for (i = 0; i <= 20; i++) {
                strikes.push(lastStrikePrice);
                lastStrikePrice = lastStrikePrice + parseInt(priceDifference);
            }
            html = "";
            $.each(strikes, function (k, v) {
                if (k != 0) {
                    html += "<div class='col-md-2 border1 ponter' onclick='addTradeModal(" + v + ")'>";
                    html += v;
                    html += "</div>";
                }
                //console.log(k,v);
            });
            $("#addTradeAllStrikes").html(html);
            pnl.push(strikes);


            var trades = localStorage.getItem("trades");
            if (trades !== null) {
                trades = JSON.parse(trades);
                html = "<thead><tr>";
                html += '<th>CalCulation</th>';
                html += '<th>Strike</th>';
                html += '<th>LTP</th>';
                html += '<th>Buy/Short</th>';
                html += '<th>CE/PE</th>';
                html += '<th>Total Lots</th>';
                html += "</tr></thead><tbody>";
                $.each(trades, function (k, t) {

                    html += "<tr id='trade-" + k + "'>";
                    if (t[5] === true) {
                        checked = "checked";
                    } else {
                        checked = "";
                    }
                    var lockIcon = '';
<?php if ($authUser) { ?>
                        /*
                         * Check if they have id from database
                         */
                        if (t[6] && t[6] !== false) {
                            lockIcon = '<i class="fa fa-lock lock-icon" onclick="removeFromDatabase(' + k + ')" aria-hidden="true"></i>';
                        } else {
                            lockIcon = '<i class="fa fa-unlock lock-icon" onclick="addToDatabase(' + k + ')" aria-hidden="true"></i>';
                        }
<?php } ?>
                    html += "<td><div class='checkbox'><label><input onchange='inCalculation(this)' " + checked + " type='checkbox' value='" + k + "'></label><span class='label label-danger' onclick='deleteTrade(" + k + ")'>Delete</span>" + lockIcon + "<div></td>";
                    html += "<td>" + t[0] + "</td>"; //strike price
                    html += "<td><input type='text' onchange=changeltp(" + k + ") id='trade-ltp-" + k + "' class='form-control' value='" + t[1] + "'></td>"; //ltp

                    html += "<td><select class='form-control' id='trade-buy-short-" + k + "' onchange=changeBuyShort(" + k + ")>";
                    if (t[2] === 'BUY') {
                        html += "<option selected value='BUY'>BUY</option><option value='SHORT'>SHORT</option>";
                    } else {
                        html += "<option value='BUY'>BUY</option><option selected value='SHORT'>SHORT</option>";
                    }
                    html += "</select></td>";

                    //html += "<td>" + t[2] + "</td>"; //buy_short
                    html += "<td>" + t[3] + "</td>"; //ce_pe
                    //html += "<td>" + t[4] + "</td>"; //number of lots
                    //start number of lots td
                    html += "<td>";
                    //html += '<div class="col-lg-12">';
                    html += '<div class="input-group">';
                    html += '<span class="input-group-btn">';
                    html += '<button type="button" class="btn btn-danger btn-number"  data-type="minus" onclick="minusbutton(' + k + ')" data-field="">';
                    html += '<span class="glyphicon glyphicon-minus"></span>';
                    html += '</button>';
                    html += '</span>';
                    html += '<input type="text" name="quantity" class="form-control" id="trade-lot-input-' + k + '" value="' + t[4] + '" min="1" max="100">';
                    html += '<span class="input-group-btn">';
                    html += '<button type="button" class="btn btn-success btn-number" data-type="plus" onclick="plusbutton(' + k + ')" data-field="">';
                    html += '<span class="glyphicon glyphicon-plus"></span>';
                    html += '</button>';
                    html += '</span>';
                    html += '</div>';
                    //html += '</div>';
                    html += "</td>";
                    //end number of lots td


                    html += "</tr>";
                    tradepnl = []
                    tradeName = t[0] + t[3] + "-" + t[2] + "[" + t[4] + " lots]";
                    if (t[5] !== true) {
                        return;
                    }
                    tradepnl.push(tradeName)
                    t[0] = parseInt(t[0]);
                    t[1] = parseFloat(t[1]);
                    t[4] = parseInt(t[4]);
                    $.each(pnl[0], function (k, v) {
                        if (k === 0) {
                            return;
                        }
                        if (t[5] !== true) {
                            return;
                        }
                        //console.log(t);
                        multiplier = t[4];
                        if (t[3] == 'CE') {
                            if (t[2] == 'BUY') {
                                if (v <= t[0]) {
                                    cal = -t[1] * multiplier;
                                } else {
                                    cal = -t[1] + (v - t[0]);
                                    cal = cal * multiplier;
                                }
                                tradepnl.push(parseFloat(cal).toFixed(2));
                            }
                            if (t['2'] == 'SHORT') {
                                if (v <= t[0]) {
                                    cal = t[1] * multiplier;
                                } else {
                                    cal = t[1] - (v - t[0]);
                                    cal = cal * multiplier;
                                }
                                tradepnl.push(parseFloat(cal).toFixed(2));
                            }
                        }


                        if (t[3] == 'PE') {
                            if (t[2] == 'BUY') {
                                if (v <= t[0]) {
                                    cal = -t[1] + (t[0] - v);
                                    cal = cal * multiplier;
                                } else {
                                    cal = -t[1] * multiplier;
                                }
                                tradepnl.push(parseFloat(cal).toFixed(2));
                            }
                            if (t['2'] == 'SHORT') {
                                if (v <= t[0]) {
                                    cal = t[1] - (t[0] - v);
                                    cal = cal * multiplier;
                                } else {
                                    cal = t[1] * multiplier;
                                }
                                tradepnl.push(parseFloat(cal).toFixed(2));
                            }
                        }


                    });
                    pnl.push(tradepnl);
                });

                html += '</tbody>';
                $('#tradesList').html(html);

                html = "";
                //console.log(pnl);

                for (i = 0; i < pnl[0].length; i++) {
                    if (i == 0) {
                        html = "<thead><tr>";
                        for (j = 0; j < pnl.length; j++) {
                            html += '<th>' + pnl[j][i] + '</th>';
                        }
                        html += '<th>PNL</th>';
                        html += '<th>Total(INR)</th>'
                        html += "</tr></thead><tbody>";
                    } else {
                        totalAmount = 0;
                        for (j = 0; j < pnl.length; j++) {
                            if (j != 0) {
                                totalAmount = totalAmount + (pnl[j][i] * totalNumberOfShareIn1Lot);
                                //totalAmount = totalAmount + (pnl[j][i] * parseFloat(trades[j - 1][4]));
                            }
                        }
                        //console.log(totalAmount);
                        if (totalAmount >= 0) {
                            c = 'success';
                        } else {
                            c = 'danger';
                        }
                        html += "<tr class='" + c + "'>";
                        pnlsum = 0;
                        for (j = 0; j < pnl.length; j++) {
                            if (j != 0) {
                                pnlsum = pnlsum + parseFloat(pnl[j][i]);
                            }
                            html += '<td>' + pnl[j][i] + '</td>';
                        }


                        html += '<td>' + parseFloat(pnlsum).toFixed(2) + '</td>';
                        html += '<td>' + parseFloat(totalAmount).toFixed(2) + '</td>';
                        html += "</tr>";
                    }
                }
                html += '</tbody>';
                $("#tradespnl").html(html);
            }
        }




    </script>

</html>
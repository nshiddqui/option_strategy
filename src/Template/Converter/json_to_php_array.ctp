<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <h3 class="my-4"><?php echo $appData->title; ?></h3>

            <?php echo $appData->content; ?>

            <div class="loading" style="display:none">Loading&#8230;</div>
            <div class="error" style="color:red"></div>
            <div class="shadow-none p-3 mb-5 bg-light rounded">
                <?php
                echo $this->Form->create('jsonToPhpArray', ['id' => 'jsonToArray']);
                echo $this->Form->control('inputJson', array('required' => true, 'type' => 'textarea', 'escape' => false, 'autocomplete' => "off", 'class' => 'form-control', 'templates' => ['inputContainer' => '<div class="form-group">{{content}}</div>']));
                echo $this->Form->button('Decode', ['class' => 'btn btn-success', 'name' => 'decode', 'value' => 'decode', 'id' => 'decode']) . ' ';
                echo $this->Form->button('Clear', ['class' => 'btn btn-primary', 'id' => 'clear', 'type' => 'button']);
                echo $this->Form->end();
                ?>
                <br>
                <?php if (isset($message) && $message['status'] && isset($jsonArray)) { ?>
                    <div class="alert alert-success">
                        <strong>Congrats!</strong> <?= $message['message']; ?>
                    </div>
                    <div class="bg-light p-3 border rounded">
                        <pre><?php var_export($jsonArray); ?></pre>
                    </div>

                <?php } else if (isset($message) && $message['status'] == false) { ?>
                    <div class="alert alert-danger">
                        <strong>OOPS!</strong> <?= $message['message']; ?>
                    </div>
                <?php } ?>
            </div>

        </div>
        <?php
        foreach ($allApps as $apps) {
            ?>
            <!-- Side Widget -->
            <div class="col-md-3">
                <div class="card my-4 mh20em">
                    <h5 class="card-header"><?php echo $apps->title; ?></h5>
                    <div class="card-body">
                        <p class="text-justify">
                            <?php
                            $content = strip_tags($apps->content);
                            echo substr($content, 0, 120);
                            if (strlen($content) >= 121) {
                                echo "...";
                            }
                            ?>
                        </p>
                    </div>
                    <div class="card-footer text-muted">
                        <?php
                        echo $this->Html->link(
                                'Give a try!', ['controller' => $apps->controller, 'action' => $apps->action], ['class' => 'btn btn-primary float-right']
                        );
                        ?>
                    </div>
                </div>
            </div>
            <?php
        }
        ?>
    </div>
</div>

<script>
    $("#clear").on('click', function () {
        $("#inputjson").val("");
    });
</script>

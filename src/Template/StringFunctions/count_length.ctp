<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <h3 class="my-4"><?php echo $appData->title; ?></h3>
            <?php echo $appData->content; ?>
            <div class="shadow-none p-3 mb-5 bg-light rounded">
                <div class="loading" style="display: none">Loading&#8230;</div>
                <?php
                echo $this->Form->create('StringFunctions', ['id' => 'countLength']);
                echo $this->Form->control('inputString', array('type' => 'textarea', 'required', 'escape' => false, 'class' => 'form-control', 'templates' => ['inputContainer' => '<div class="form-group">{{content}}</div>']));
                echo $this->Form->button('Count', ['class' => 'btn btn-success', 'id' => 'count']) . ' ';
                echo $this->Form->button('Clear', ['class' => 'btn btn-primary', 'id' => 'clear', 'type' => 'button']) . ' ';
                echo $this->Form->control('whitespace', array('type' => 'checkbox', 'label' => ' Include white space', 'checked' => true, 'templates' => ['inputContainer' => '{{content}}']));
                echo $this->Form->button('Show me an example', ['class' => 'btn btn-primary float-right', 'id' => 'example', 'type' => 'button']);
                echo $this->Form->end();
                ?>
                <br>
                <div class="form-group">
                    <label for="usr">Character Count (Output):</label>
                    <input type="text" class="form-control" id="characterOutput" readonly="true">
                </div> 
                <div class="form-group">
                    <label for="usr">Word Count (Output):</label>
                    <input type="text" class="form-control" id="wordCountOutput" readonly="true">
                </div> 

            </div>
        </div>
        <?php
        foreach ($allApps as $apps) {
            ?>
            <!-- Side Widget -->
            <div class="col-md-3">
                <div class="card my-4 mh20em">
                    <h5 class="card-header"><?php echo $apps->title; ?></h5>
                    <div class="card-body">
                        <p class="text-justify">
                            <?php
                            $content = strip_tags($apps->content);
                            echo substr($content, 0, 120);
                            if(strlen($content)>=121){
                                echo "...";
                            }
                            ?>
                        </p>
                    </div>
                    <div class="card-footer text-muted">
                        <?php
                        echo $this->Html->link(
                                'Give a try!', ['controller' => $apps->controller, 'action' => $apps->action], ['class' => 'btn btn-primary float-right']
                        );
                        ?>
                    </div>
                </div>
            </div>
            <?php
        }
        ?>
    </div>
</div>

<script>
    $(document).ready(function () {
        $("#clear").on('click', function () {
            $("#inputstring").val("");
        });

        $("#example").on('click', function () {
            $("#inputstring").val("this is an example.");
            $("#count").click();
        });

        $("#count").on('click', function (e) {
            e.preventDefault();
            $.ajax({
                type: 'POST',
                url: "<?php echo $this->Url->build(array('controller' => 'stringFunctions', 'action' => 'countLength')); ?>",
                data: $('#countLength').serialize(),
                dataType: 'json',
                beforeSend: function (formData, jqForm, options) {
                    $('.loading').css('display', 'block');
                },
                success: function (resp) {
                    $("#characterOutput").val(resp.length);
                    $("#wordCountOutput").val(resp.totalWord);
                    $('html, body').animate({
                        scrollTop: $("#characterOutput").offset().top
                    }, 2000);
                    $('.loading').css('display', 'none');
                }
            });
        });
    });
</script>
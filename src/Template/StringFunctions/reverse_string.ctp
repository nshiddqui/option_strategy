<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <h3 class="my-4"><?php echo $appData->title; ?></h3>
            <?php echo $appData->content; ?>
            <div class="shadow-none p-3 mb-5 bg-light rounded">
                <div class="loading" style="display: none">Loading&#8230;</div>
                <div class="msg"></div>
                <?php
                echo $this->Form->create('StringFunctions', ['id' => 'reverseString']);

                echo $this->Form->control('inputString', array('type' => 'textarea', 'required', 'escape' => false, 'class' => 'form-control', 'templates' => ['inputContainer' => '<div class="form-group">{{content}}</div>']));
                echo $this->Form->button('Reverse', ['class' => 'btn btn-success', 'id' => 'reverse']) . ' ';
                echo $this->Form->button('Clear', ['class' => 'btn btn-primary', 'id' => 'clear', 'type' => 'button']) . ' ';

                echo $this->Form->end();
                ?>
                <br>
                <div class="form-group">
                    <label for="usr">Output:</label>
                    <textarea class="form-control" readonly="true" id="output"></textarea>
                </div> 

            </div>
        </div>
        <?php
        foreach ($allApps as $apps) {
            ?>
            <!-- Side Widget -->
            <div class="col-md-3">
                <div class="card my-4 mh20em">
                    <h5 class="card-header"><?php echo $apps->title; ?></h5>
                    <div class="card-body">
                        <p class="text-justify">
                            <?php
                            $content = strip_tags($apps->content);
                            echo substr($content, 0, 120);
                            if(strlen($content)>=121){
                                echo "...";
                            }
                            ?>
                        </p>
                    </div>
                    <div class="card-footer text-muted">
                        <?php
                        echo $this->Html->link(
                                'Give a try!', ['controller' => $apps->controller, 'action' => $apps->action], ['class' => 'btn btn-primary float-right']
                        );
                        ?>
                    </div>
                </div>
            </div>
            <?php
        }
        ?>
    </div>
</div>


<script>
    $(document).ready(function () {
        $("#clear").on('click', function () {
            $("#inputstring").val("");
            $(".msg").html("");
        });

        $("#example").on('click', function () {
            $("#inputstring").val("this is an example.");
            $("#reverse").click();
        });

        $("#reverse").on('click', function (e) {
            e.preventDefault();
            $('.loading').css('display', 'block');
            var input = $("#inputstring").val();
            if (input === "") {
                $(".msg").html('<div class="alert alert-danger"><strong>OOPS!</strong> Please provide a input to test.</div>');
            } else {
                var reverse = input.split('').reverse().join('');
                if (input === reverse) {
                    $(".msg").html('<div class="alert alert-success"><strong>Success!</strong> This is a Palindrome String.</div>');
                } else {
                    $(".msg").html('<div class="alert alert-danger"><strong>OOPS!</strong> This string is not a Palindrome String.</div>');
                }
                $("#output").val(reverse);
            }
            $('.loading').css('display', 'none');

        });
    });
</script>


<?php echo $this->Html->script('beautifiers/jsonBeautifier', ['block' => 'script']); ?>
<?php echo $this->Html->css('beautifiers/jsonBeautifier', ['block' => 'css']); ?>
<style>
    .json-beautifier-textarea{
        height: 85vh !important;
        border-radius: 0px;
        border: 1px solid silver;
        box-shadow: inset 0 1px 3px #e4e4e4;
        font-size: 12px;
        line-height: 18px;
        font-family: Monaco,Menlo,'Ubuntu Mono',Consolas,source-code-pro,'Droid Sans Mono',monospace;
    }
    .linenumber{
        background: url(../img/beautifiers/linenumber.png);
        background-attachment: local;
        background-repeat: no-repeat;
        padding-left: 35px;
        padding-top: 10px;
        border-color:#ccc;
    }

    .scroll-y{
        overflow-y: scroll; 
    }
</style>
<div class="row" itemscope itemtype="http://schema.org/Service">
    <meta itemprop="serviceType" content="JSON beautifier" />
    <div class="col-md-5 p-1">
        <div class="form-group">
            <span id="box_label_expand" style="opacity: 0.8;">Expand</span>
            <textarea class="form-control json-beautifier-textarea linenumber" placeholder="JSON INPUT" id="input"></textarea>
        </div>
    </div>
    <div class="col-md-5 p-1">
        <div class="form-group">
            <div class="form-control json-beautifier-textarea scroll-y" id="output"></div>
        </div>
    </div>
    <div class="col-md-2 p-1">
        <!-- vertical banner -->
        <ins class="adsbygoogle"
             style="display:inline-block;width:100%;height:84vh"
             data-ad-client="ca-pub-9302256880608736"
             data-ad-slot="2622018473"></ins>
        <script>
            (adsbygoogle = window.adsbygoogle || []).push({});
        </script>
    </div>
</div>
<script>
    $(document).ready(function () {
        $(".container-fluid").removeClass('my-4');
        $("#input").on('change', function () {
            var json = $(this).val();
            $("#output").jsonBeautifier(json);
        });
        $('#input').on('paste', function (e) {
            var pasteData = e.originalEvent.clipboardData.getData('text');
            $("#output").jsonBeautifier(pasteData);
        });

    });

</script>
<div class="container">
    <div class="row">
        <div class="col-md-6 offset-md-3 my-4">
            <div class="card">
                <div class="card-header text-center">
                    <h3>Login</h3>
                </div>
                <div class="card-body">
                    <?= $this->Flash->render() ?>
                    <?php echo $this->Form->create('User'); ?>
                    <div class="form-group">
                        <?php echo $this->Form->control('email',['class'=>'form-control']); ?>
                    </div>
                    <div class="form-group">
                        <?php echo $this->Form->control('password',['class'=>'form-control']); ?>
                    </div>
                    <?php echo $this->Form->button('Login',['class'=>'btn btn-primary']); ?>
                    <?= $this->Form->end() ?>
                </div> 

            </div>
        </div>

    </div>
</div>

<div class="container">
    <div class="row">
        <?php foreach ($allApps as $apps) { ?>
            <div class="col-md-4">
                <div class="card mb-4 mh20em">
                    <div class="card-header">
                        <h2 class="card-title" style="font-size:24px"><?php echo $apps->title; ?></h2>
                    </div>
                    
                    <div class="card-body">
                        <p class="card-text text-justify">
                            <!-- 220 Character limit -->
                            <?php
                            $content = strip_tags($apps->content);
                            echo substr($content, 0, 200);
                            if(strlen($content)>=201){
                                echo "...";
                            }
                            
                            ?>
                        </p>
                    </div>
                    <div class="card-footer text-muted">
                        <?php
                        echo $this->Html->link(
                                'Give a try!', ['controller' => $apps->controller, 'action' => $apps->action], ['class' => 'btn btn-primary float-right']
                        );
                        ?>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>
</div>

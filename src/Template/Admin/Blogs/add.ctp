<div class="blogs form large-10 medium-9 columns content">
    <?= $this->Form->create($blog) ?>
    <fieldset>
        <legend><?= __('Add Blog') ?></legend>
        <?php
        echo $this->Form->control('topic_id', ['options' => $topics, 'empty' => true]);
        echo $this->Form->control('title');
        echo $this->Form->control('content');
        echo $this->Form->control('youtube_link');
        echo $this->Form->control('download_link');
        echo $this->Form->control('user_id', ['options' => $users]);
        echo $this->Form->control('status', ['options' => [0 => 'Draft', 1 => 'Publish']]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
<!-- 4.5.7 -->
<?php echo $this->Html->script('/ckeditor/ckeditor.js', ['block' => 'script']); ?>

<script>
    jQuery(document).ready(function ($) {
        var editor = CKEDITOR.replace('content');
        /*var editor = CKEDITOR.replace('content',
                {
                    height: 400,
                    filebrowserBrowseUrl: basePath + 'kcfinder/browse.php?type=files',
                    filebrowserImageBrowseUrl: basePath + 'kcfinder/browse.php?type=images',
                    filebrowserFlashBrowseUrl: basePath + 'kcfinder/browse.php?type=flash',
                    filebrowserUploadUrl: basePath + 'kcfinder/upload.php?type=files',
                    filebrowserImageUploadUrl: basePath + 'kcfinder/upload.php?type=images',
                    filebrowserFlashUploadUrl: basePath + 'kcfinder/upload.php?type=flash'
                });*/
    });
</script>

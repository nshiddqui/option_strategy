<div class="blogs form large-10 medium-9 columns content">
    <?= $this->Form->create($blog) ?>
    <fieldset>
        <legend><?= __('Edit Blog') ?></legend>
        <?php
        echo $this->Form->control('topic_id', ['options' => $topics, 'empty' => true]);
        echo $this->Form->control('title');
        echo $this->Form->control('content');
        echo $this->Form->control('youtube_link');
        echo $this->Form->control('download_link');
        echo $this->Form->control('user_id', ['options' => $users]);
        echo $this->Form->control('status', ['options' => [0 => 'Draft', 1 => 'Publish']]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
<?php echo $this->Html->script('/ckeditor/ckeditor.js', ['block' => 'script']); ?>
<script>
    jQuery(document).ready(function ($) {
        CKEDITOR.replace('content');
    });
</script>

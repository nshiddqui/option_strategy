<div class="topics view large-10 medium-9 columns content">
    <h3><?= h($topic->title) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Parent Topic') ?></th>
            <td><?= $topic->has('parent_topic') ? $this->Html->link($topic->parent_topic->title, ['controller' => 'Topics', 'action' => 'view', $topic->parent_topic->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Title') ?></th>
            <td><?= h($topic->title) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Slug') ?></th>
            <td><?= h($topic->slug) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($topic->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Lft') ?></th>
            <td><?= $this->Number->format($topic->lft) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Rght') ?></th>
            <td><?= $this->Number->format($topic->rght) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Views') ?></th>
            <td><?= $this->Number->format($topic->views) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($topic->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($topic->modified) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Blogs') ?></h4>
        <?php if (!empty($topic->blogs)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Title') ?></th>
                <th scope="col"><?= __('Content') ?></th>
                <th scope="col"><?= __('Youtube Link') ?></th>
                <th scope="col"><?= __('Download Link') ?></th>
                <th scope="col"><?= __('Topic Id') ?></th>
                <th scope="col"><?= __('User Id') ?></th>
                <th scope="col"><?= __('Views') ?></th>
                <th scope="col"><?= __('Slug') ?></th>
                <th scope="col"><?= __('Status') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($topic->blogs as $blogs): ?>
            <tr>
                <td><?= h($blogs->id) ?></td>
                <td><?= h($blogs->title) ?></td>
                <td><?= h($blogs->content) ?></td>
                <td><?= h($blogs->youtube_link) ?></td>
                <td><?= h($blogs->download_link) ?></td>
                <td><?= h($blogs->topic_id) ?></td>
                <td><?= h($blogs->user_id) ?></td>
                <td><?= h($blogs->views) ?></td>
                <td><?= h($blogs->slug) ?></td>
                <td><?= h($blogs->status) ?></td>
                <td><?= h($blogs->created) ?></td>
                <td><?= h($blogs->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Blogs', 'action' => 'view', $blogs->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Blogs', 'action' => 'edit', $blogs->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Blogs', 'action' => 'delete', $blogs->id], ['confirm' => __('Are you sure you want to delete # {0}?', $blogs->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Topics') ?></h4>
        <?php if (!empty($topic->child_topics)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Parent Id') ?></th>
                <th scope="col"><?= __('Lft') ?></th>
                <th scope="col"><?= __('Rght') ?></th>
                <th scope="col"><?= __('Title') ?></th>
                <th scope="col"><?= __('Slug') ?></th>
                <th scope="col"><?= __('Views') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($topic->child_topics as $childTopics): ?>
            <tr>
                <td><?= h($childTopics->id) ?></td>
                <td><?= h($childTopics->parent_id) ?></td>
                <td><?= h($childTopics->lft) ?></td>
                <td><?= h($childTopics->rght) ?></td>
                <td><?= h($childTopics->title) ?></td>
                <td><?= h($childTopics->slug) ?></td>
                <td><?= h($childTopics->views) ?></td>
                <td><?= h($childTopics->created) ?></td>
                <td><?= h($childTopics->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Topics', 'action' => 'view', $childTopics->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Topics', 'action' => 'edit', $childTopics->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Topics', 'action' => 'delete', $childTopics->id], ['confirm' => __('Are you sure you want to delete # {0}?', $childTopics->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>

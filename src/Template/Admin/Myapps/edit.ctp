<div class="apps form large-10 medium-9 columns content">
    <?= $this->Form->create($app) ?>
    <fieldset>
        <legend><?= __('Edit App') ?></legend>
        <?php
        echo $this->Form->control('title');
        echo $this->Form->control('content');
        echo $this->Form->control('controller');
        echo $this->Form->control('action');
        echo $this->Form->control('variable');
        echo $this->Form->control('meta');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
<?php echo $this->Html->script('/ckeditor/ckeditor.js', ['block' => 'script']); ?>
<script>
    jQuery(document).ready(function ($) {
        CKEDITOR.replace('content');
    });
</script>


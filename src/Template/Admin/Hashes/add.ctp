<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Hash $hash
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Hashes'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="hashes form large-9 medium-8 columns content">
    <?= $this->Form->create($hash) ?>
    <fieldset>
        <legend><?= __('Add Hash') ?></legend>
        <?php
            echo $this->Form->control('value');
            echo $this->Form->control('md5_hash');
            echo $this->Form->control('sha1_hash');
            echo $this->Form->control('rank');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>

<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Hash[]|\Cake\Collection\CollectionInterface $hashes
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Hash'), ['action' => 'add']) ?></li>
    </ul>
</nav>
<div class="hashes index large-9 medium-8 columns content">
    <h3><?= __('Hashes') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('value') ?></th>
                <th scope="col"><?= $this->Paginator->sort('md5_hash') ?></th>
                <th scope="col"><?= $this->Paginator->sort('sha1_hash') ?></th>
                <th scope="col"><?= $this->Paginator->sort('rank') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($hashes as $hash): ?>
            <tr>
                <td><?= $this->Number->format($hash->id) ?></td>
                <td><?= h($hash->value) ?></td>
                <td><?= h($hash->md5_hash) ?></td>
                <td><?= h($hash->sha1_hash) ?></td>
                <td><?= $this->Number->format($hash->rank) ?></td>
                <td><?= h($hash->created) ?></td>
                <td><?= h($hash->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $hash->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $hash->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $hash->id], ['confirm' => __('Are you sure you want to delete # {0}?', $hash->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>

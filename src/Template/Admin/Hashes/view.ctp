<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Hash $hash
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Hash'), ['action' => 'edit', $hash->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Hash'), ['action' => 'delete', $hash->id], ['confirm' => __('Are you sure you want to delete # {0}?', $hash->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Hashes'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Hash'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="hashes view large-9 medium-8 columns content">
    <h3><?= h($hash->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Value') ?></th>
            <td><?= h($hash->value) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Md5 Hash') ?></th>
            <td><?= h($hash->md5_hash) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Sha1 Hash') ?></th>
            <td><?= h($hash->sha1_hash) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($hash->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Rank') ?></th>
            <td><?= $this->Number->format($hash->rank) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($hash->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($hash->modified) ?></td>
        </tr>
    </table>
</div>

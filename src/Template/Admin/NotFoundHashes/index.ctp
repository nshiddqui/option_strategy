<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\NotFoundHash[]|\Cake\Collection\CollectionInterface $notFoundHashes
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Not Found Hash'), ['action' => 'add']) ?></li>
    </ul>
</nav>
<div class="notFoundHashes index large-9 medium-8 columns content">
    <h3><?= __('Not Found Hashes') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('value') ?></th>
                <th scope="col"><?= $this->Paginator->sort('email') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($notFoundHashes as $notFoundHash): ?>
            <tr>
                <td><?= $this->Number->format($notFoundHash->id) ?></td>
                <td><?= h($notFoundHash->value) ?></td>
                <td><?= h($notFoundHash->email) ?></td>
                <td><?= h($notFoundHash->created) ?></td>
                <td><?= h($notFoundHash->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $notFoundHash->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $notFoundHash->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $notFoundHash->id], ['confirm' => __('Are you sure you want to delete # {0}?', $notFoundHash->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>

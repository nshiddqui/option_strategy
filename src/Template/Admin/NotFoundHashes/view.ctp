<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\NotFoundHash $notFoundHash
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Not Found Hash'), ['action' => 'edit', $notFoundHash->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Not Found Hash'), ['action' => 'delete', $notFoundHash->id], ['confirm' => __('Are you sure you want to delete # {0}?', $notFoundHash->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Not Found Hashes'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Not Found Hash'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="notFoundHashes view large-9 medium-8 columns content">
    <h3><?= h($notFoundHash->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Value') ?></th>
            <td><?= h($notFoundHash->value) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Email') ?></th>
            <td><?= h($notFoundHash->email) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($notFoundHash->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($notFoundHash->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($notFoundHash->modified) ?></td>
        </tr>
    </table>
</div>

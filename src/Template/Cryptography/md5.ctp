<div class="container">
    <div class="row">
        <div class="col-lg-9">
            <div class="shadow-none p-3 mb-5 bg-light rounded">
                <div class="loading" style="display: none">Loading&#8230;</div>
                <h3 class="my-4"><?php echo $appData->title; ?></h3>
                <?= $this->Flash->render() ?>
                <div class="error"></div>
                <?php echo $this->Form->create('md5', ['id' => 'cryptography']); ?>
                <?php
                $this->Form->unlockField('type');
                echo $this->Form->control('inputString', array('required' => true, 'onkeyup' => "this.value=removeSpaces(this.value);", 'escape' => false, 'autocomplete' => "off", 'class' => 'form-control', 'templates' => ['inputContainer' => '<div class="form-group">{{content}}</div>']));
                ?>
                <div class="form-group">
                    <label for="output">Output:</label>
                    <input type="text" class="form-control" readonly="true" id="output">
                </div>
                <?php
                echo $this->Form->button('Encrypt', ['class' => 'btn btn-success', 'value' => 'encrypt', 'id' => 'encrypt']) . ' ';
                echo $this->Form->button('Decrypt', ['class' => 'btn btn-success', 'value' => 'decrypt', 'id' => 'decrypt']) . ' ';
                ?>
                <?= $this->Form->end() ?>



                <?php echo $this->Form->create('hashNotFound', ['url' => ['controller' => 'Cryptography', 'action' => 'hashNotFoundEmail'], 'type' => 'GET', 'id' => 'hashNotFound', 'style' => 'display:none']); ?>
                <?php
                echo $this->Form->control('inputString', array('required' => true, 'onkeyup' => "this.value=removeSpaces(this.value);", 'escape' => false, 'autocomplete' => "off", 'class' => 'form-control', 'readonly' => true, 'templates' => ['inputContainer' => '<div class="form-group">{{content}}</div>']));
                echo $this->Form->control('email', array('required' => true, 'onkeyup' => "this.value=removeSpaces(this.value);", 'escape' => false, 'autocomplete' => "off", 'class' => 'form-control', 'templates' => ['inputContainer' => '<div class="form-group">{{content}}</div>']));

                echo $this->Form->control('requestid', array('required' => true, 'type' => "hidden", 'escape' => false));
                ?>
                <?php
                echo $this->Form->button('Request for another database.', ['class' => 'btn btn-success', 'id' => 'sendRequest']) . ' ';
                ?>

                <?= $this->Form->end() ?>

            </div>
            <?php
            echo $appData->content;
            ?>
        </div>
        <div class="col-md-3">
            <?php
            $i = 0;
            foreach ($allApps as $apps) {
                ?>
                <!-- Side Widget -->
                <div class="card <?php echo $i != 0 ? 'my-4' : ''; ?> mh20em">
                    <h5 class="card-header"><?php echo $apps->title; ?></h5>
                    <div class="card-body">
                        <p class="text-justify">
                            <?php
                            $content = strip_tags($apps->content);
                            echo substr($content, 0, 130);
                            if(strlen($content)>=131){
                                echo "...";
                            }
                            ?>
                        </p>
                    </div>
                    <div class="card-footer text-muted">
                        <?php
                        echo $this->Html->link(
                                'Give a try!', ['controller' => $apps->controller, 'action' => $apps->action], ['class' => 'btn btn-primary float-right']
                        );
                        ?>
                    </div>
                </div>
                <?php
                $i = $i + 1;
                if ($i == 2) {
                    break;
                }
            }
            ?>
        </div>
        <!-- next loop -->
        <?php
        $j = 0;
        foreach ($allApps as $apps) {
            $j = $j + 1;
            if ($j <= $i) {
                continue;
            }
            ?>
            <!-- Side Widget -->
            <div class="col-md-3">
                <div class="card <?php echo $i != 0 ? 'my-4' : ''; ?> mh20em">
                    <h5 class="card-header"><?php echo $apps->title; ?></h5>
                    <div class="card-body">
                        <p class="text-justify">
                            <?php
                            $content = strip_tags($apps->content);
                            echo substr($content, 0, 120);
                            if(strlen($content)>=121){
                                echo "...";
                            }
                            ?>
                        </p>
                    </div>
                    <div class="card-footer text-muted">
                        <?php
                        echo $this->Html->link(
                                'Give a try!', ['controller' => $apps->controller, 'action' => $apps->action], ['class' => 'btn btn-primary float-right']
                        );
                        ?>
                    </div>
                </div>
            </div>
            <?php
        }
        ?>
    </div>
</div>
<script>
    function removeSpaces(string) {
        return string.split(' ').join('');
    }
    $(document).ready(function () {
        $("#encrypt").on('click', function (e) {
            e.preventDefault();
            $.ajax({
                type: 'POST',
                url: "<?php echo $this->Url->build(array('controller' => 'Cryptography', 'action' => 'md5')); ?>",
                data: $("#cryptography").serialize() + "&type=encrypt",
                dataType: 'json',
                beforeSend: function (xhr) {
                    $('.loading').css('display', 'block');
                },
                success: function (resp) {
                    if (resp.status) {
                        $("#output").val(resp.md5);
                    } else {
                        $("#output").val("");
                        $(".error").html('<div class="alert alert-danger alert-dismissible fade show"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Error !</strong> ' + resp.message + '</div>');
                    }
                    $('.loading').css('display', 'none');
                }
            });
        });


        $("#decrypt").on('click', function (e) {
            e.preventDefault();
            $.ajax({
                type: 'POST',
                url: "<?php echo $this->Url->build(array('controller' => 'Cryptography', 'action' => 'md5')); ?>",
                data: $("#cryptography").serialize() + "&type=decrypt",
                dataType: 'json',
                beforeSend: function (xhr) {
                    $('.loading').css('display', 'block');
                },
                success: function (resp) {
                    if (resp.status) {
                        $(".error").html("");
                        $("#output").val(resp.md5);
                    } else {
                        if (typeof (resp.id) !== 'undefined') {
                            $("#cryptography").remove();
                            $("#hashNotFound").css('display', 'block');
                            $("#requestid").val(resp.id);
                            $("#inputstring").val(resp.hash);
                        }
                        $("#output").val("");
                        $(".error").html('<div class="alert alert-danger alert-dismissible fade show"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Error !</strong> ' + resp.message + '</div>');
                    }
                    $('.loading').css('display', 'none');
                }
            });
        });
    });
</script>

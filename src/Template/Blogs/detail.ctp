<div class="container">
    <div class="row">
        <div class="col-lg-9">
            <?php if($blog->youtube_link != NULL){?>
            <!-- youtube video iframe -->
            <?php } ?>
            <?php echo $blog->content; ?>
        </div>
        <div class="col-md-3">
            <?php
            $first = true;
            foreach ($allApps as $apps) {
                ?>
                <!-- Side Widget -->
                <div class="card <?php echo $first ? 'my-4' : ''; ?> mh20em">
                    <h5 class="card-header"><?php echo $apps->title; ?></h5>
                    <div class="card-body">
                        <p class="text-justify">
                            <?php
                            $content = strip_tags($apps->content);
                            echo substr($content, 0, 130);
                            if (strlen($content) >= 131) {
                                echo "...";
                            }
                            ?>
                        </p>
                    </div>
                    <div class="card-footer text-muted">
                        <?php
                        echo $this->Html->link(
                                'Give a try!', ['controller' => $apps->controller, 'action' => $apps->action], ['class' => 'btn btn-primary float-right']
                        );
                        ?>
                    </div>
                </div>
                <?php
                $first = false;
            }
            ?>
        </div>
    </div>
</div>

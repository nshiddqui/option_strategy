<div class="container">
    <div class="row">
        <div class="col-lg-9">
            <div class="shadow-none p-3 mb-5 bg-light rounded">
                <div class="loading" style="display: none">Loading&#8230;</div>
                <h3 class="my-4"><?php echo $appData->title; ?></h3>
                <?= $this->Flash->render() ?>
                <div class="error"></div>
                <?php echo $this->Form->create('', ['type' => 'get','id' => 'ipinfo']); ?>
                <?php

                echo $this->Form->control('ip', array('required' => true, 'placeholder' => 'Search any ip address here...', 'onkeyup' => "this.value=removeSpaces(this.value);", 'escape' => false, 'autocomplete' => "off", 'class' => 'form-control', 'templates' => ['inputContainer' => '<div class="form-group">{{content}}</div>']));
                ?>
                <?php
                echo $this->Form->button('Get IP Info', ['class' => 'btn btn-success']) . ' ';
                ?>
                <hr>
                <div class="form-group">
                    <label for="output">IP detail for <?php echo $ip; ?>:</label>
                    <table class="table table-striped" id="ipdet">
                        <tbody>
                        <?php
                        if($ipDetail){
                        foreach ($ipDetail as $k => $d) { ?>
                            <tr>
                                <th><?php echo ucwords(str_replace("_", " ", $k)); ?></th>
                                <td>
                                    <?php
                                    if (is_array($d)) {
                                        if ($k == 'sub_divisions') {
                                            echo implode(", ", $d);
                                        } else {
                                            foreach ($d as $kd => $dd) {
                                                echo ucwords(str_replace("_", " ", $kd)) . ':' . $dd . '<br>';
                                            }
                                        }

                                    } else {
                                        echo ucwords(str_replace("_", " ", $k)) . ':' . $d;
                                    }

                                    ?>
                                </td>

                            </tr>
                        <?php } }else{ ?>
                            <div class="alert alert-danger">
                                <strong>OOPS!</strong> Invalid ip or unable to capture the data.
                            </div>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>

                <?= $this->Form->end() ?>
            </div>
            <?php
            echo $appData->content;
            ?>
        </div>
        <div class="col-md-3">
            <?php
            $i = 0;
            foreach ($allApps as $apps) {
                ?>
                <!-- Side Widget -->
                <div class="card <?php echo $i != 0 ? 'my-4' : ''; ?> mh20em">
                    <h5 class="card-header"><?php echo $apps->title; ?></h5>
                    <div class="card-body">
                        <p class="text-justify">
                            <?php
                            $content = strip_tags($apps->content);
                            echo substr($content, 0, 130);
                            if (strlen($content) >= 131) {
                                echo "...";
                            }
                            ?>
                        </p>
                    </div>
                    <div class="card-footer text-muted">
                        <?php
                        echo $this->Html->link(
                            'Give a try!', ['controller' => $apps->controller, 'action' => $apps->action], ['class' => 'btn btn-primary float-right']
                        );
                        ?>
                    </div>
                </div>
                <?php
                $i = $i + 1;
                if ($i == 2) {
                    break;
                }
            }
            ?>
        </div>
        <!-- next loop -->
        <?php
        $j = 0;
        foreach ($allApps as $apps) {
            $j = $j + 1;
            if ($j <= $i) {
                continue;
            }
            ?>
            <!-- Side Widget -->
            <div class="col-md-3">
                <div class="card <?php echo $i != 0 ? 'my-4' : ''; ?> mh20em">
                    <h5 class="card-header"><?php echo $apps->title; ?></h5>
                    <div class="card-body">
                        <p class="text-justify">
                            <?php
                            $content = strip_tags($apps->content);
                            echo substr($content, 0, 120);
                            if (strlen($content) >= 121) {
                                echo "...";
                            }
                            ?>
                        </p>
                    </div>
                    <div class="card-footer text-muted">
                        <?php
                        echo $this->Html->link(
                            'Give a try!', ['controller' => $apps->controller, 'action' => $apps->action], ['class' => 'btn btn-primary float-right']
                        );
                        ?>
                    </div>
                </div>
            </div>
            <?php
        }
        ?>
    </div>
</div>
<script>
    function removeSpaces(string) {
        return string.split(' ').join('');
    }
</script>

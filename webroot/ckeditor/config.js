/**
 * @license Copyright (c) 2003-2016, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function (config) {
    // Define changes to default configuration here. For example:
    // config.language = 'fr';
    // config.uiColor = '#AADC6E';
    config.filebrowserBrowseUrl = basePath + '/kcfinder/browse.php?opener=ckeditor&type=files';
    config.filebrowserImageBrowseUrl = basePath + '/kcfinder/browse.php?opener=ckeditor&type=images';
    config.filebrowserFlashBrowseUrl = basePath + '/kcfinder/browse.php?opener=ckeditor&type=flash';
    config.filebrowserUploadUrl = basePath + '/kcfinder/upload.php?opener=ckeditor&type=files';
    config.filebrowserImageUploadUrl = basePath + '/kcfinder/upload.php?opener=ckeditor&type=images';
    config.filebrowserFlashUploadUrl = basePath + '/kcfinder/upload.php?opener=ckeditor&type=flash';
};
